# Config_prop_web

Aplicación centralizada para dar manejo de las propiedades de IPOS de manera fácil,agil y sin errores.

---
### Algunas tecnologías a usar
- Git
- Java 1.8
- Maven 3
- Posgresql
- Springboot
- Angular
---

### Idea

Api rest que soporte la interacción con una base de datos central y todas las bases de datos de las sucursales.
App Angular para manejar el frontend y la interacción con el usuario.

---
### Requests/Response

---
##### url base: http://localhost:9093/configuradorweb/
---
#### isLive 

*Endpoint:* /isLive

##### Request: GET

##### Response: 200 OK
---
#### isReady

*Endpoint:* /isReady

##### Request: GET

##### Response: 200 OK
---
#### Busqueda de propiedad

*Endpoint:* /query

##### Request:

```json
{
query: "<valor_a_buscar>"
numero_sucursales:[<n_suc_1,n_suc_2,...,n_suc_n>]
}
```
##### Response:

```json
[
    {
        "propertyName": "mostrar_detalle_promociones",
        "propertyValue": null,
        "descripcionName": null,
        "editable": "false",
        "sucursal": null
    }
]
```

**sucursales**: indica las sucursales donde buscar.
---
#### Update property sucursales

*Endpoint:* /udpateProperties

##### Request:

```json
{
	"clave":"<nombre_clave>",
	"valor":"<nomre_valor>",
	"numero_sucursales":[<nro_suc1>,<nro_suc2>,...,<nro_sucn>]
}
```

##### Response:
```
Status 200 OK
```
---
#### Query por sucursal

*Endpoint:* /queryForSuc

##### Request:

```json
{
	"query":"mostrar",
	"numero_sucursales":[<nro_suc1>,<nro_suc2>,...,<nro_sucn>]
}
```

##### Response:

```json
[
    {
        "propertyName": "intellipos.mostrar.promociones.asociados",
        "propertyValue": "false",
        "descripcionName": null,
        "editable": null,
        "sucursal": "<nro_sucn>"
    },
    {
        "propertyName": "intellipos.mostrar.promociones.asociados.complementarios",
        "propertyValue": "false",
        "descripcionName": null,
        "editable": null,
        "sucursal": "<nro_sucn>"
    }
]
```
---

#### 