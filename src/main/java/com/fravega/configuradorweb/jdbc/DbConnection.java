package com.fravega.configuradorweb.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.fravega.configuradorweb.config.DbConfiguration;

public class DbConnection {

	static DbConnection instancia = null;
	static String password;
	static String user;
	static String driver;
	static String url;

	static final Logger logger = Logger.getLogger(DbConnection.class.getName());

	public DbConnection() {
		super();
	}

	/**
	 * Por defecto la conexión es sin auto commit.
	 * @param ipSucursal
	 * @return Connection
	 * @throws Exception
	 */
	public static Connection getJdbcConnection(String ipSucursal,DbConfiguration dbconfig) throws Exception {
		init();
		driver = dbconfig.getDriver();
		url = dbconfig.getUrl().replaceAll("<ip:port>", ipSucursal);
		user = dbconfig.getUser();
		password = dbconfig.getPassword();
		Class.forName(driver);
		DriverManager.setLoginTimeout(5);
		Connection conn = null;
		
		logger.info(String.format("JdbcConnection - %s - %s",driver,url));
		try {
			conn = DriverManager.getConnection(String.format(url, ipSucursal), user, password);
		} catch (SQLException e) {
			logger.warning(
					String.format("getJdbcConnection - Error al obtener la conexion a la base - " + e.getMessage()));
		}
		conn.setAutoCommit(false);
		return conn;
	}

	public static void init() {
		driver = StringUtils.EMPTY;
		url = StringUtils.EMPTY;
		user = StringUtils.EMPTY;
		password = StringUtils.EMPTY;
	}
}
