package com.fravega.configuradorweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fravega.configuradorweb.entity.ConfiguracionProperty;

public interface ConfiguracionPropertiesRepository extends JpaRepository<ConfiguracionProperty,Long> {
	
	List<ConfiguracionProperty> findByClaveContaining(String clave);
}