package com.fravega.configuradorweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fravega.configuradorweb.entity.ConfiguracionPropertyControl;

public interface ConfiguracionPropertiesControlRepository extends JpaRepository<ConfiguracionPropertyControl,Long>{

	@SuppressWarnings("unchecked")
	ConfiguracionPropertyControl save(ConfiguracionPropertyControl cpc);

	@Query(value="select * from configuracion_properties_control where clave =  ?1 order by  modification_date desc limit 1",nativeQuery=true)
	ConfiguracionPropertyControl findLastByClave(String claveToSearch);
}
