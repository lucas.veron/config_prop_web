package com.fravega.configuradorweb.rest;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.fravega.configuradorweb.model.ConfiguracionPropertyDto;
import com.fravega.configuradorweb.model.QueryDto;
import com.fravega.configuradorweb.model.UpdateDto;
import com.fravega.configuradorweb.service.ConfiguracionPropertiesService;

@Path("/configuradorweb")
@CrossOrigin(origins = { "*" }, maxAge = 4800, allowCredentials = "false")
public class ConfiguradorWebServiceRest {
	private final Logger logger = Logger.getLogger(ConfiguradorWebServiceRest.class.getName());

	private final static String ENDPOINT_IS_READY = "/IsReady";
	private final static String ENDPOINT_IS_LIVE = "/isLive";
	private final static String ENDPOINT_QUERY = "/query";
	private final static String ENDPOINT_QUERY_FOR_SUC = "/queryForSuc";
	private final static String ENDPOINT_UPDATE_PROPERTIES = "/udpateProperties";

	private static final String ENDPOINT_SAVE_PROP_CONTROL = "/savePropControl";

	private final static String LOGGER_ENDPOINT_MSG = " endpoint : status ";

	@Autowired
	private ConfiguracionPropertiesService propertyServices;

//	@GET
//	@Path("/recargarProperties")
//	@Produces(MediaType.TEXT_XML)
//	public String recargarProperties() {
//		Response response = Response.status(200).build();
//		String response = null;
//		try {
//			response = propertyServices.recargarProperties();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return response;
//	}

	@GET
	@Path(ENDPOINT_SAVE_PROP_CONTROL)
	public Response savePropControl() {
		Response response = Response.status(200).build();

		// TODO: ver que hacer con esto!
		// TODO: ver manejo de casos de error
		/* Random String for testing len=8,useLetter=true,useNumber=false */
		String generatedString = RandomStringUtils.random(8, true, false);

		propertyServices.saveChangeInPropertyControl(1234, "test-clave0", generatedString);

		logger.log(Level.INFO, ENDPOINT_SAVE_PROP_CONTROL + LOGGER_ENDPOINT_MSG + response.getStatus());

		return response;
	}

	@GET
	@Path(ENDPOINT_IS_READY)
	public Response isReady() {
		Response response = Response.status(200).build();
		logger.log(Level.INFO, ENDPOINT_IS_READY + LOGGER_ENDPOINT_MSG + response.getStatus());
		// TODO: realizar una conexion a la base para ver si responde OK!
		return response;
	}

	@GET
	@Path(ENDPOINT_IS_LIVE)
	public Response isLive() {
		Response response = Response.status(200).build();
		/* Estoy vivo si soy capaz de responder 200 simplemente */
		logger.log(Level.INFO, ENDPOINT_IS_READY + LOGGER_ENDPOINT_MSG + response.getStatus());
		return response;
	}

	@POST
	@Path(ENDPOINT_QUERY)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response retrievePropertiesFrom(QueryDto queryDto) {
		/*
		 * me conecto a central para traer el listado de properties disponibles
		 */
		List<ConfiguracionPropertyDto> propertyDtos = null;
		if (queryDto != null && queryDto.getQuery() != null) {
			propertyDtos = propertyServices.retrievePropertiesForWord(queryDto.getQuery());
		}
		Response response = Response.status(200).entity(propertyDtos).build();
		logger.log(Level.INFO, ENDPOINT_QUERY + LOGGER_ENDPOINT_MSG + response.getStatus());
		return response;
	}

	@POST
	@Path(ENDPOINT_QUERY_FOR_SUC)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response retrievePropertiesForSuc(QueryDto queryDto) {
		/*
		 * me conecto a central para traer el listado de properties disponibles
		 */
		Collection<ConfiguracionPropertyDto> propertyDtos = null;
		Response response = null;
		if (queryDto != null && queryDto.getQuery() != null) {
			propertyDtos = propertyServices.retrievePropertiesForSuc(queryDto);
		}
		if (propertyDtos == null || propertyDtos.isEmpty()) {
			response = Response.status(404).build();
		}
		response = Response.status(200).entity(propertyDtos).build();
		logger.log(Level.INFO, ENDPOINT_QUERY_FOR_SUC + LOGGER_ENDPOINT_MSG + response.getStatus());

		return response;
	}

	@POST
	@Path(ENDPOINT_UPDATE_PROPERTIES)
	@Produces(MediaType.APPLICATION_JSON)
	public Response UpdateProperties(UpdateDto updateDto) {

		Response response = Response.status(400).build();
		if (updateValido(updateDto)) {
			if (queryConValorDefinido(updateDto)) {
				response = Response.status(200).build();
				propertyServices.updateProperties(updateDto);
			}
		}
		logger.log(Level.INFO, ENDPOINT_UPDATE_PROPERTIES + LOGGER_ENDPOINT_MSG + response.getStatus());
		return response;
	}

	private boolean queryConValorDefinido(UpdateDto updateDto) {
		return !StringUtils.isEmpty(updateDto.getValor().trim());
	}

	private boolean updateValido(UpdateDto updateDto) {
		return updateDto != null && updateDto.getClave() != null && updateDto.getNumero_sucursales() != null
				&& !updateDto.getNumero_sucursales().isEmpty();
	}
}