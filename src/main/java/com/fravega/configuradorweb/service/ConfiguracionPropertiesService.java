package com.fravega.configuradorweb.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.fravega.configuradorweb.config.DbConfiguration;
import com.fravega.configuradorweb.config.ResolverSucursal;
import com.fravega.configuradorweb.entity.ConfiguracionProperty;
import com.fravega.configuradorweb.entity.ConfiguracionPropertyControl;
import com.fravega.configuradorweb.jdbc.DbConnection;
import com.fravega.configuradorweb.model.ConfiguracionPropertyDto;
import com.fravega.configuradorweb.model.QueryDto;
import com.fravega.configuradorweb.model.Sucursal;
import com.fravega.configuradorweb.model.UpdateDto;
import com.fravega.configuradorweb.repository.ConfiguracionPropertiesControlRepository;
import com.fravega.configuradorweb.repository.ConfiguracionPropertiesRepository;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class ConfiguracionPropertiesService {

	private final Logger logger = Logger.getLogger(ConfiguracionPropertiesService.class.getName());

	/* Query para consultar existencia de property en central */
	private static final String SQL_SELECT_PROPERTY_CENTRAL = "SELECT clave FROM intellipos.configuracion_properties where clave = ?";

	/* Queries usadas para insertar nuevas properties en la base */
	private static final String SQL_INSERT_NEW_PROPERTY = "INSERT INTO intellipos.configuracionproperties(creation_date,modification_date,clave,value)"
			+ " SELECT now(),now(),?,? WHERE ? NOT IN (SELECT clave FROM intellipos.configuracionproperties)";
	private static final String SQL_UPDATE_IDEMPRESA_NEW_PROPERTY = "UPDATE intellipos.configuracionproperties "
			+ "SET idEmpresa = cast('180'||'000000000' as numeric(19))+id WHERE idEmpresa IS NULL AND clave= ?";

	/* Querie de consulta */
	private final String SQL_SELECT_WORD_CONFIG_PROPERTIES = "select * from intellipos.configuracionproperties where clave like ?";
	/* Querie de actualizacion */
	private final String SQL_UPDATE_CONFIG_PROPERTIES = "update intellipos.configuracionproperties set modification_date=now(),value = ? where clave = ?";

	private final static String CLAVE = "clave";
	private final static String PROPERTY_NAME = "propertyName";
	private static final String DESCRIPCION = "descripcion";
	private static final String DESCRIPTION_NAME = "descripcionName";
	private static final String IS_EDITABLE = "editable";
	private static final String TIPO_EDICION = "permiteEdicion";

	private static final String CONTENT_TYPE = "content-type";
	private static final String MEDIA_TYPE_TEXT_XML = "text/xml";
	private static final String BODY_SOAP_RECARGAR_PROPERTY = "<x:Envelope xmlns:x=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.intellipos.hasar.com/\">\n    <x:Header/>\n    <x:Body>\n        <web:recargarConfiguracionProperties/>\n    </x:Body>\n</x:Envelope>";
	private static final String SOAP_URL_RECARGAR_PROPERTY = "http://%s/ipos-web/IntelliposWebService";

	private static final String CACHE_CONTROL = "cache-control";
	private static final String NO_CACHE = "no-cache";

	@Autowired
	private ConfiguracionPropertiesRepository repository;

	@Autowired
	private ConfiguracionPropertiesControlRepository repositoryControl;

	@Autowired
	private ResolverSucursal resolverSucursal;

	@Autowired
	private DbConfiguration dbconfiguration;

	public List<ConfiguracionPropertyDto> retrievePropertiesForWord(String word) {
		List<ConfiguracionProperty> props = new ArrayList<ConfiguracionProperty>();
		Collection<ConfiguracionPropertyDto> propDtos = new ArrayList<ConfiguracionPropertyDto>();

		logger.info("Recuperando properties según palabra: " + word);
		try {
			props = repository.findByClaveContaining(word);

			propDtos = configurationToDto(props);

		} catch (DataAccessException e) {
			logger.log(Level.SEVERE, String.format("Error al buscar properties: %s", e.getMessage()));
			// TODO: modificar para devolver 1 error
			return createEmptyProp();
		}
		return (List<ConfiguracionPropertyDto>) propDtos;
	}

	// TODO: Tratar errores para los casos donde no existen las sucursales
	// solicitadas configuradas
	public Collection<ConfiguracionPropertyDto> retrievePropertiesForSuc(QueryDto queryDto) {
		String regex = String.format("%%%s%%", queryDto.getQuery());

		Collection<ConfiguracionPropertyDto> confProps = new ArrayList<ConfiguracionPropertyDto>();
		for (Integer nsuc : queryDto.getNumero_sucursales()) {
			Sucursal suc = new Sucursal(nsuc);
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				conn = DbConnection.getJdbcConnection(resolverSucursal.getIpBase(suc), dbconfiguration);
				logger.info(String.format("Conexión sucursal %s OK", nsuc));

				ps = conn.prepareStatement(SQL_SELECT_WORD_CONFIG_PROPERTIES);
				/* %clave% */
				ps.setString(1, regex);

				rs = ps.executeQuery();
				while (rs.next()) {
					ConfiguracionPropertyDto cpd = new ConfiguracionPropertyDto();
					cpd.setSucursal(String.valueOf(nsuc));
					cpd.setPropertyName(rs.getString("clave"));
					cpd.setPropertyValue(rs.getString("value"));
					confProps.add(cpd);
				}
			} catch (Exception e) {
				logger.log(Level.SEVERE,
						String.format("Error al buscar properties de sucursales - %s", e.getMessage()));
				e.printStackTrace();
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						logger.log(Level.SEVERE, String.format("Error al cerrar la consulta - %s", e.getMessage()));
						e.printStackTrace();
					}
				}
				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						logger.log(Level.SEVERE,
								String.format("Error al cerrar el statement de la consulta - %s", e.getMessage()));
						e.printStackTrace();
					}
				}
				try {
					if (conn != null) {
						conn.close();
						logger.info("Se cerro la conexion a la base de datos sucursal, " + suc);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE,
							String.format("Error al intentar cerrar la conexión a la base de datos, sucursal %s - %s",
									suc, e.getMessage()));
					e.printStackTrace();
				}
			}

		}
		return confProps;
	}

	private Collection<ConfiguracionPropertyDto> configurationToDto(List<ConfiguracionProperty> props) {
		MapperFactory mapperFactory = getConfigurationMapper();
		Collection<ConfiguracionPropertyDto> propDtos = new ArrayList<ConfiguracionPropertyDto>();
		if (props != null && !props.isEmpty()) {
			for (ConfiguracionProperty prop : props) {
				MapperFacade mapper = mapperFactory.getMapperFacade();
				ConfiguracionPropertyDto propDto = new ConfiguracionPropertyDto();
				mapper.map(prop, propDto);
				propDtos.add(propDto);
			}
		} else {
			return createEmptyProp();
		}
		return propDtos;
	}

	private List<ConfiguracionPropertyDto> createEmptyProp() {
		List<ConfiguracionPropertyDto> propDtos = new ArrayList<ConfiguracionPropertyDto>();

		propDtos.add(new ConfiguracionPropertyDto());
		return propDtos;
	}

	/* Definimos el Mapper */
	private MapperFactory getConfigurationMapper() {
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(ConfiguracionProperty.class, ConfiguracionPropertyDto.class).field(CLAVE, PROPERTY_NAME)
				.field(DESCRIPCION, DESCRIPTION_NAME).field(TIPO_EDICION, IS_EDITABLE).register();
		return mapperFactory;
	}

	// TODO: verificar que existe la propiedad usando JPA
	// TODO: legajoUser hardcodeado!
	public void updateProperties(UpdateDto updateDto) {
		int legajoUser = 1234;

		/* Chequeo que efectivamente existe la propiedad conectandome a central */
		boolean validProperty = validProperty(updateDto);

		if (validProperty) {
			String valueNew = updateDto.getValor();
			String claveToChange = updateDto.getClave();
			boolean savePropertyControl = false;
			for (Integer nsuc : updateDto.getNumero_sucursales()) {
				Sucursal suc = new Sucursal(nsuc);
				Connection conn = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				try {
					conn = DbConnection.getJdbcConnection(resolverSucursal.getIpBase(suc), dbconfiguration);
					logger.info(String.format("Conectado a sucursal %s", suc.getNumero_sucursal()));
					// TODO logger
					ps = conn.prepareStatement(SQL_UPDATE_CONFIG_PROPERTIES);
					/* valor(value) */
					ps.setString(1, valueNew);
					/* clave */
					ps.setString(2, claveToChange);

					int i = ps.executeUpdate();
					logger.info(String.format("update query, %s rows updating", i));

					if (i == 0) {
						insertNewProperty(updateDto, conn, nsuc);
					} else { /* Se logró 1 update correctamente */
						conn.commit();
						recargarProperties(resolverSucursal.getIpIpos(suc));
						savePropertyControl = true;
					}

				} catch (Exception e) {
					/* Falta rollback acá ? */
					logger.log(Level.SEVERE, String.format("Error al buscar los datos - %s", e.getMessage()));
					e.printStackTrace();
				} finally {
					if (rs != null) {
						try {
							rs.close();
						} catch (SQLException e) {
							logger.log(Level.SEVERE, String.format("Error al cerrar la consulta - %s", e.getMessage()));
							e.printStackTrace();
						}
					}
					if (ps != null) {
						try {
							ps.close();
						} catch (SQLException e) {
							logger.log(Level.SEVERE,
									String.format("Error al cerrar el statement de la consulta - %s", e.getMessage()));
							e.printStackTrace();
						}
					}
					try {
						if (conn != null) {
							conn.close();
							logger.info("Se cerro la conexion a la base de datos sucursal, " + suc);
						}
					} catch (SQLException e) {
						logger.log(Level.SEVERE,
								String.format(
										"Error al intentar cerrar la conexión a la base de datos, sucursal %s - %s",
										suc, e.getMessage()));
						e.printStackTrace();
					}
				}
			}
			/*
			 * Al hacer 1 update en alguna de las sucursales actualizo la tabla de control
			 */
			if (savePropertyControl) {
				saveChangeInPropertyControl(legajoUser, claveToChange, valueNew);
			}
		}
	}

	/**
	 * Valida si existe la property en central. En caso de error retorna false.
	 * TODO: usar JPA repository para hacer la consulta a central.
	 * 
	 * @param updateDto
	 * @return boolean
	 */
	private boolean validProperty(UpdateDto updateDto) {
		String clave = updateDto.getClave();
		Connection conn;
		int count = 0;
		try {
			conn = DbConnection.getJdbcConnection(resolverSucursal.getIpCentral(), dbconfiguration);
			PreparedStatement ps = conn.prepareStatement(SQL_SELECT_PROPERTY_CENTRAL);
			ps.setString(1, updateDto.getClave());
			ResultSet rs = ps.executeQuery();
			/* rs.getRow() ? */
			while (rs.next()) {
				count++;
			}
			if (count > 0) {
				logger.info(String.format("Property '%s' encontrada en central", clave));
				return true;
			}
			logger.info(String.format("Property '%s' NO existente en central!", clave));
			return false;
		} catch (Exception e) {
			logger.severe(String.format("Error al consultar existencia de property '%s' en central", clave));
			e.printStackTrace();
			return false;
		}
	}

	private void insertNewProperty(UpdateDto updateDto, Connection conn, Integer nsuc) throws SQLException {
		PreparedStatement ps;
		String clave = updateDto.getClave();

		ps = conn.prepareStatement(SQL_INSERT_NEW_PROPERTY);
		ps.setString(1, clave);
		ps.setString(2, updateDto.getValor());
		ps.setString(3, clave);

		int i = ps.executeUpdate();
		if (i == 1) {
			logger.info(String.format("Nueva propiedad insertada, suc %s", nsuc));
			/* Update de las configuraciones */
			ps = conn.prepareStatement(SQL_UPDATE_IDEMPRESA_NEW_PROPERTY);
			ps.setString(1, updateDto.getClave());

			i = ps.executeUpdate();
			if (i == 1) {
				logger.info(String.format("Propiedad '%s' actualizada correctamente, suc %s", clave, nsuc));
				conn.commit();
			} else {
				throw new SQLException("Error al actualizar la nueva propiedad, %s", clave);
			}
		} else {
			throw new SQLException("Error al insertar la nueva propiedad, %s", clave);
		}
	}

	/**
	 * Gaurda en la base de datos de control la modificación realizada en la
	 * property
	 * 
	 * @param legajoUser
	 * @param claveToSearch
	 * @param valueToSet
	 */
	public void saveChangeInPropertyControl(int legajoUser, String claveToSearch, String valueToSet) {

		ConfiguracionPropertyControl cpcSaved = repositoryControl.findLastByClave(claveToSearch);

		ConfiguracionPropertyControl cpcNew = new ConfiguracionPropertyControl();
		cpcNew.setModificationLegajo(legajoUser);
		cpcNew.setClave(claveToSearch);
		cpcNew.setValueCurrent(valueToSet);
		cpcNew.setValueOld(cpcSaved == null ? null : cpcSaved.getValueCurrent());

		cpcNew.setCreationDate(new Date());
		cpcNew.setModificationDate(new Date());

		repositoryControl.save(cpcNew);

		logger.info(String.format("Nueva 'property control' guardada: %s", cpcNew.toString()));
	}

	public int recargarProperties(String ipSuc) throws IOException {

		OkHttpClient client = new OkHttpClient();

		com.squareup.okhttp.MediaType mediaType = MediaType.parse(MEDIA_TYPE_TEXT_XML);
		RequestBody body = RequestBody.create(mediaType, BODY_SOAP_RECARGAR_PROPERTY);
//				"<x:Envelope xmlns:x=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.intellipos.hasar.com/\">\n    <x:Header/>\n    <x:Body>\n        <web:recargarConfiguracionProperties/>\n    </x:Body>\n</x:Envelope>");
		String url = String.format(SOAP_URL_RECARGAR_PROPERTY, ipSuc);
		Request request = new Request.Builder().url(url).post(body).addHeader(CONTENT_TYPE, MEDIA_TYPE_TEXT_XML)
				.addHeader(CACHE_CONTROL, NO_CACHE).build();

		com.squareup.okhttp.Response response = client.newCall(request).execute();
		return response.code();
	}
}
