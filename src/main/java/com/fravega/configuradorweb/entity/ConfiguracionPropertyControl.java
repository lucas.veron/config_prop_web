package com.fravega.configuradorweb.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity
@Data
@Table(name="configuracion_properties_control",uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
public class ConfiguracionPropertyControl {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="creation_date")
	private Date creationDate;
	
	@Column(name="modification_date")
	private Date modificationDate;
	
	@Column(name="clave")
	private String clave;
	
	@Column(name="modification_legajo")
	private int modificationLegajo;
	
	@Column(name="value_old")
	private String valueOld;
	
	@Column(name="value_current")
	private String valueCurrent;
}
