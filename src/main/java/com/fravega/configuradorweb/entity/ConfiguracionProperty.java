package com.fravega.configuradorweb.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity
@Data
@Table(name="configuracion_properties",uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
public class ConfiguracionProperty {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="creation_date")
	private Date creationDate;
	
	@Column(name="modification_date")
	private Date modificationDate;
	
	@Column(name="clave")
	private String clave;
	
	@Column(name="tipo_dato")
	private String tipoDato;
	
	@Column(name="permite_edicion")
	private boolean permiteEdicion;
	
	@Column(name="descripcion")
	private String descripcion;
}
