package com.fravega.configuradorweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class ConfiguradorWeb {
	public static void main(String[] args) {
		SpringApplication.run(ConfiguradorWeb.class,args);
	}
}