package com.fravega.configuradorweb.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.fravega.configuradorweb.model.Sucursal;

@Configuration
@PropertySource("file:${app.config.suc}/sucursales.properties")
public class ResolverSucursal {

	private static final String KEY_IP_CENTRAL = "0";
	
	private static final String SPLIT_REGEX = "\\|";
	
	private static final String PORT_BASE_CENTRAL = "5436";
	private static final String PORT_BASE = "5432";
	private static final String PORT_IPOS = "8080";	

	private static enum Ips {
		IP_LEGADOS, IP_IPOS, IP_BASE
	};

	@Autowired
	private Environment env;

	@Bean
	Sucursal getSucursal() {
		return new Sucursal();
	}

	private String getIp(Sucursal suc, Ips ip) throws Exception {
		String sSuc = String.valueOf(suc.getNumero_sucursal());
		String ipsSuc = env.getProperty(sSuc);
		/* Recupero la fila con ipLegados|ipIpos|ipBase */

		String ipSuc = ipsSuc.split(SPLIT_REGEX)[getNumeroIp(ip)];
		if (ipSuc == null) {
			throw new Exception(String.format(
					"Error al recuperar ip, sucursal %s. Ver archivo de configuración 'sucursales.properties'", sSuc));
		}
		return ipSuc;
	}

	private int getNumeroIp(Ips ip) {
		switch (ip) {
		case IP_LEGADOS:
			return 0;
		case IP_IPOS:
			return 1;
		case IP_BASE:
			return 2;
		/* return ip central */
		default:
			return 3;
		}
	}

	public String getIpCentral() throws Exception {
		return env.getProperty(KEY_IP_CENTRAL).split(SPLIT_REGEX)[2] + ":" + PORT_BASE_CENTRAL;
	}

	public String getIpIpos(Sucursal suc) throws Exception {
		return getIp(suc, Ips.IP_IPOS) + ":" + PORT_IPOS;
	}
	
	public String getIpBase(Sucursal suc) throws Exception {
		return getIp(suc, Ips.IP_BASE) + ":" + PORT_BASE;
	}
}