package com.fravega.configuradorweb.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.fravega.configuradorweb.rest.ConfiguradorWebServiceRest;

@Configuration
public class JaxRsConfig extends ResourceConfig {
	public JaxRsConfig() {
		/* JaxRs Configuration */
		register(ConfiguradorWebServiceRest.class);
		
		CorsFilter corsFilter = new CorsFilter();
        corsFilter.getAllowedOrigins().add("*");
		register(corsFilter);
		/* jpa repository */
//		register(ConfiguracionPropertiesRepository.class);
	}
}