package com.fravega.configuradorweb.model;

import java.util.Collection;

import lombok.Data;

@Data
public class PropertyResponse {
	private Collection<String> clave;
}
