package com.fravega.configuradorweb.model;

import lombok.Data;

@Data
public class ConfiguracionPropertyDto {
	private String propertyName;
	private String propertyValue;
	private String descripcionName;
	private String editable;
	private String sucursal;
}
