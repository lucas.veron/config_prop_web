package com.fravega.configuradorweb.model;

import java.util.Collection;

import lombok.Data;

@Data
public class UpdateDto {
	private String clave;
	private String valor;
	private Collection<Integer> numero_sucursales;
}
