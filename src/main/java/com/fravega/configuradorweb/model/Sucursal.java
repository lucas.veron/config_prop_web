package com.fravega.configuradorweb.model;

import lombok.Data;

@Data
public class Sucursal {
	public Sucursal() {
		super();
	}

	public Sucursal(int numeroSucursal) {
		super();
		this.numero_sucursal = numeroSucursal;
	}

	private int numero_sucursal;
}
