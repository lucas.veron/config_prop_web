package com.fravega.configuradorweb.model;

import java.util.List;

import lombok.Data;

@Data
public class QueryDto {
	/* palabra a formar parte de la query */
	private String query;
	/* lista de sucursales(nro) donde realizar el query */
	private List<Integer> numero_sucursales;
}
